module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        date: grunt.template.date(Date(), 'yyyy'),
        clean: ['angular-simple-sidebar.min.css', 'angular-simple-sidebar-black-theme.min.css', 'angular-simple-sidebar.min.js'],
        cssmin: {
            main: {
                files: {
                    'angular-simple-sidebar.min.css': ['app/directive/angular-simple-sidebar.css'],
                    'angular-simple-sidebar-black-theme.min.css': ['app/directive/theme/angular-simple-sidebar-black-theme.css']
                }
            }
        },
        uglify: {
            options: {
                compress: true
            },
            applib: {
                src: ['app/directive/angular-simple-sidebar.js'],
                dest: 'angular-simple-sidebar.min.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default', ['clean', 'cssmin', 'uglify']);
};
